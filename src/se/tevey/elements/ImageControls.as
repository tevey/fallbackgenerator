package se.tevey.elements
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import se.tevey.events.ObjectEvent;
	
	public class ImageControls extends Sprite
	{
		public static const EVENT_SAVE_PNG:String = "event/savepng";
		public static const EVENT_SAVE_JPEG:String = "event/savejpeg";
		
		private var _saveJpegBtn:DefaultButton;
		private var _savePngBtn:DefaultButton;
		private var _title:TextField;
		private var _titlePng:TextField;
		private var _quality:InputField;
		private var _bg:Sprite;
		
		public function ImageControls()
		{
			super();
			init();
		}
		
		private function init():void
		{
			
			_bg = new Sprite();
			_bg.graphics.beginFill(Main.DARK_COLOR);
			_bg.graphics.drawRect(0,0, Main.SIDEBAR_WIDTH,100);
			_bg.graphics.endFill();
			_bg.graphics.beginFill(Main.LIGHT_COLOR);
			_bg.graphics.drawRect(0,0,Main.SIDEBAR_WIDTH, 20);
			
			var titleFormat:TextFormat = new TextFormat();
			titleFormat.color = Main.DARK_COLOR;
			titleFormat.font = "Trebuchet MS, Arial, Helvetica, sans-serif";
			titleFormat.size = 12;
			
			_title = new TextField();
			_title.autoSize = TextFieldAutoSize.LEFT;
			_title.defaultTextFormat = titleFormat;
			_title.text = "jpeg/png";
			_title.x = Main.MARGIN;
			
			_quality = new InputField("Quality(0-100):", 100, "Quality: 0=worst, 100=best");
			_quality.addEventListener(InputField.EVENT_INPUT_FOCUS, onInputFocus);
			_quality.y = 20 + Main.MARGIN;
			_quality.x = Main.MARGIN;
			
			_saveJpegBtn = new DefaultButton("Save Jpeg", onSaveJpeg);
			_saveJpegBtn.y = _quality.y + _quality.height + 10;
			_saveJpegBtn.x = Main.MARGIN;
			
			_savePngBtn = new DefaultButton("Save Png", onSavePng);
			_savePngBtn.y = _saveJpegBtn.y;
			_savePngBtn.x = Main.SIDEBAR_WIDTH - _savePngBtn.width - Main.MARGIN;
			
			addChild(_bg);
			addChild(_title);
			addChild(_quality);
			addChild(_saveJpegBtn);
			addChild(_savePngBtn);
		}
		
		private function onInputFocus(event:ObjectEvent):void
		{
			dispatchEvent(new ObjectEvent(InputField.EVENT_INPUT_FOCUS, event.object));
		}
		
		private function onSaveJpeg(me:MouseEvent):void
		{
			dispatchEvent(new Event(EVENT_SAVE_JPEG));
		}
		
		private function onSavePng(me:MouseEvent):void
		{
			dispatchEvent(new Event(EVENT_SAVE_PNG));
		}
		
		public function getQuality():int
		{
			return Math.max(0, Math.min(parseInt(_quality.inputValue), 100));
		}
	}
}