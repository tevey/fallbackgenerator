package se.tevey.elements
{
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	public class BannerInfo extends Sprite
	{
		public static const EVENT_NEXT_BANNER:String = "event/nextbanner";
		
		private var _title:TextField;
		private var _info:TextField;
		private var _bg:Sprite;
		private var _nextBtn:DefaultButton;
		
		public function BannerInfo()
		{
			super();
			init();
		}
		
		private function init():void
		{
			
			_bg = new Sprite();
			_bg.graphics.beginFill(Main.DARK_COLOR);
			_bg.graphics.drawRect(0,0, Main.SIDEBAR_WIDTH,180);
			_bg.graphics.endFill();
			_bg.graphics.beginFill(Main.LIGHT_COLOR);
			_bg.graphics.drawRect(0,0,Main.SIDEBAR_WIDTH, 20);
			
			var titleFormat:TextFormat = new TextFormat();
			titleFormat.align = TextFormatAlign.CENTER;
			titleFormat.color = Main.DARK_COLOR;
			titleFormat.font = "Trebuchet MS, Arial, Helvetica, sans-serif";
			titleFormat.size = 12;
			
			_title = new TextField();
			_title.defaultTextFormat = titleFormat;
			_title.text = "swf info";
			_title.x = Main.MARGIN;
			_title.autoSize = TextFieldAutoSize.LEFT;
			
			var infoFormat:TextFormat = new TextFormat();
			infoFormat.color = Main.LIGHT_COLOR;
			infoFormat.font = "Trebuchet MS, Arial, Helvetica, sans-serif";
			infoFormat.size = 12;
			
			_info = new TextField();
			_info.defaultTextFormat = infoFormat;
			_info.multiline = true;
			_info.width = 180;
			_info.height = 118;
			_info.x = Main.MARGIN;
			_info.y = _title.y + _title.height + 5;
			
			_nextBtn = new DefaultButton("Next Banner", onNextDown);
			_nextBtn.x = (Main.SIDEBAR_WIDTH - _nextBtn.width)*0.5
			_nextBtn.y = _bg.height - _nextBtn.height - Main.MARGIN;
			
			addChild(_bg);
			addChild(_title);
			addChild(_info);
			addChild(_nextBtn);
		}
		
		private function onNextDown(me:MouseEvent):void
		{
			dispatchEvent(new Event(EVENT_NEXT_BANNER));
		}
		
		public function clear():void
		{
			_info.text = "";
		}
		
		public function setData(info:LoaderInfo, name:String):void
		{
			_info.text = "";
			_info.htmlText += "Name: " + name + "\n";
			_info.htmlText += "Size: " + (info.bytesTotal/1024).toString().substr(0,4) + " KB\n";
			_info.htmlText += "SWF Version: " + info.swfVersion + "\n";
			_info.htmlText += "AS Version: " + info.actionScriptVersion + "\n";
			_info.htmlText += "Framerate: " + info.frameRate + "\n";
			_info.htmlText += "Dimensions: " + info.width + "x" + info.height;
		}
	}
}