package se.tevey.elements
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	import se.tevey.DebugConsole;
	
	public class Queue extends Sprite
	{
		
		private var _bg:Sprite;
		private var _title:TextField;
		private var _queue:TextField;
		private var _scroll:Scrollbar;
		private var _scrollMask:Sprite;
		private var _queueHolder:Sprite;
		
		public function Queue()
		{
			super();
			init();
		}
		
		private function init():void
		{
			_bg = new Sprite();
			addChild(_bg);
			
			_scrollMask = new Sprite();
			_scrollMask.graphics.beginFill(0xff0000);
			_scrollMask.graphics.drawRect(0,0,Main.SIDEBAR_WIDTH, 20);
			_scrollMask.graphics.endFill();
			_scrollMask.y = 20;
			addChild(_scrollMask);
			
			var titleFormat:TextFormat = new TextFormat();
			titleFormat.color = Main.DARK_COLOR;
			titleFormat.font = "Trebuchet MS, Arial, Helvetica, sans-serif";
			titleFormat.size = 12;
			
			_title = new TextField();
			_title.autoSize = TextFieldAutoSize.LEFT;
			_title.defaultTextFormat = titleFormat;
			_title.text = "files";
			_title.x = Main.MARGIN;
			addChild(_title);
			
			_queueHolder = new Sprite();
			_queueHolder.x = 10;
			_queueHolder.y = 20;
			addChild(_queueHolder);
			
			var queueFormat:TextFormat = new TextFormat();
			queueFormat.color = Main.LIGHT_COLOR;
			queueFormat.font = "Trebuchet MS, Arial, Helvetica, sans-serif";
			queueFormat.size = 12;
			
			_queue = new TextField();
			_queue.defaultTextFormat = queueFormat;
			_queue.multiline = true;
			_queue.autoSize = TextFieldAutoSize.LEFT;
			/*_queue.y = 20;
			_queue.x = 10;*/
			//_queue.border = true;
			//_queue.borderColor = 0xffff00;
			_queueHolder.addChild(_queue);
			
			
			
			_scroll = new Scrollbar();
			_scroll.addEventListener(Scrollbar.EVENT_SCROLL_UPDATE, onScroll);
			_scroll.y = 20;
			_scroll.x = Main.SIDEBAR_WIDTH - _scroll.width;
			addChild(_scroll);
			
			_queueHolder.mask = _scrollMask;
		}
		
		private function onScroll(e:Event):void
		{
			_queueHolder.y = 20-((_queueHolder.height + 20 - _bg.height) * _scroll.getPercent());
		}
		
		public function updateHeight(h:Number):void
		{
			_bg.graphics.clear();
			_bg.graphics.beginFill(Main.DARK_COLOR);
			_bg.graphics.drawRect(0,0, Main.SIDEBAR_WIDTH,stage.stageHeight - this.y);
			_bg.graphics.endFill();
			_bg.graphics.beginFill(Main.LIGHT_COLOR);
			_bg.graphics.drawRect(0,0,Main.SIDEBAR_WIDTH, 20);
			_scrollMask.height = _bg.height - 20;
			updateScroll();
		}
		
		public function updateScroll():void
		{
			if(_queueHolder.height <= _bg.height - 20)
			{
				_scroll.visible = false;
				_queueHolder.y = 20;
				DebugConsole.add("SnAp TO TOP");
			}
			else
			{
				_scroll.visible = true;
				_scroll.updateHeight(_bg.height - 20, _queueHolder.height, _queueHolder.y-20);
			}
		}
		
		public function addToQueue(name:String, index:int):void
		{
			_queue.htmlText += (index + 1) + ". " + name + "\n";
			updateScroll();
		}
		
		public function clear():void
		{
			_queue.text = "";
			updateScroll();
		}
	}
}