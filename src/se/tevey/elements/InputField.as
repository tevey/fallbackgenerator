package se.tevey.elements
{
	import flash.display.Sprite;
	import flash.events.FocusEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	
	import se.tevey.events.ObjectEvent;
	
	public class InputField extends Sprite
	{
		public static const EVENT_INPUT_FOCUS:String = "event/inputfocus";
		private var _label:TextField;
		private var _input:TextField;
		private var _message:String;
		
		public function InputField(label:String, defaultValue:Number, message:String = "")
		{
			super();
			_message = message;
			var labelFormat:TextFormat = new TextFormat();
			labelFormat.color = Main.LIGHT_COLOR;
			labelFormat.font = "Trebuchet MS, Arial, Helvetica, sans-serif";
			labelFormat.size = 12;
			
			_label = new TextField();
			_label.defaultTextFormat = labelFormat;
			_label.text = label;
			_label.autoSize = TextFieldAutoSize.LEFT;
			addChild(_label);
			
			
			var inputFormat:TextFormat = new TextFormat();
			inputFormat.color = Main.DARK_COLOR;
			inputFormat.font = "Trebuchet MS, Arial, Helvetica, sans-serif";
			inputFormat.size = 12;
			
			_input = new TextField();
			_input.defaultTextFormat = inputFormat;
			_input.restrict = "0123456789";
			_input.type = TextFieldType.INPUT;
			_input.width = 85;
			_input.height = 22;
			_input.text = defaultValue.toString();
			_input.background = true;
			_input.backgroundColor = Main.LIGHT_COLOR;
			_input.addEventListener(FocusEvent.FOCUS_IN, onFocus);
			addChild(_input);
			
			_input.x = 90;
		}
		
		private function onFocus(event:FocusEvent):void
		{
			dispatchEvent(new ObjectEvent(EVENT_INPUT_FOCUS, _message));
		}
		
		public function enable():void
		{
			_input.selectable = true;
			_input.backgroundColor = Main.LIGHT_COLOR;
		}
		
		public function disable():void
		{
			_input.selectable = false;
			_input.backgroundColor = 0x666666;
		}
		
		public function get inputValue():String
		{
			return _input.text;
		}
	}
}