package se.tevey.elements
{
	import com.greensock.TweenMax;
	import com.greensock.plugins.AutoAlphaPlugin;
	import com.greensock.plugins.TweenPlugin;
	
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	public class HelpWindow extends Sprite
	{
		private var _info:TextField;
		private var _bg:Sprite;
		
		public function HelpWindow()
		{
			super();
			TweenPlugin.activate([AutoAlphaPlugin]);
			init();
		}
		
		private function init():void
		{
			hideMessage(true);
			_bg = new Sprite();
			_bg.graphics.beginFill(Main.LIGHT_COLOR);
			_bg.graphics.drawRoundRect(0, 0, 500, 30, 3, 3);
			_bg.graphics.endFill();
			addChild(_bg);
			
			var infoFormat:TextFormat = new TextFormat();
			infoFormat.color = Main.DARK_COLOR;
			infoFormat.font = "Trebuchet MS, Arial, Helvetica, sans-serif";
			infoFormat.size = 12;
			
			_info = new TextField();
			_info.defaultTextFormat = infoFormat;
			_info.multiline = false;
			_info.width = 496;
			_info.height = 30;
			_info.selectable = false;
			_info.x = 3;
			_info.y = 3;
			
			addChild(_info);
		}
		
		public function showMessage(message:String):void
		{
			_info.text = message;
			TweenMax.killAll();
			TweenMax.to(this, 0.5, {autoAlpha:1, onComplete:hideMessage, onCompleteParams:[false]});
		}
		
		public function hideMessage(instant:Boolean):void
		{
			if(instant)
			{
				this.visible = false;
				this.alpha = 0;	
			}
			else
			{
				TweenMax.to(this, 0.5, {autoAlpha:0, delay:4});
			}
		}
	}
}