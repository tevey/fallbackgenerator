package se.tevey.elements
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import se.tevey.DebugConsole;
	
	public class Scrollbar extends Sprite
	{
		public static const EVENT_SCROLL_UPDATE:String = "event/scrollupdate";
		private var _handle:Sprite;
		private var _bg:Sprite;
		private var _localY:Number;
		private var _scrollEvent:Event;
		
		public function Scrollbar()
		{
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			_scrollEvent = new Event(EVENT_SCROLL_UPDATE);
			
			_bg = new Sprite();
			_bg.graphics.beginFill(Main.LIGHT_COLOR);
			_bg.graphics.drawRect(0,0,10,20);
			_bg.graphics.endFill();
			addChild(_bg);
			
			_handle = new Sprite();
			_handle.buttonMode = true;
			_handle.graphics.beginFill(Main.DARK_COLOR);
			_handle.graphics.drawRect(5,0,5,20);
			_handle.graphics.endFill();
			_handle.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			addChild(_handle);
		}
		
		private function onAddedToStage(event:Event):void
		{
			DebugConsole.add("ADDED");
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}
		
		private function onMouseDown(event:MouseEvent):void
		{
			_localY = event.localY;
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		private function onMouseUp(event:MouseEvent):void
		{
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		private function onEnterFrame(event:Event):void
		{
			_handle.y = Math.max(0, Math.min(mouseY - _localY, _bg.height-_handle.height));
			dispatchEvent(_scrollEvent);
		}
		
		public function getPercent():Number
		{
			return _handle.y/(_bg.height - _handle.height);
		}
		
		public function updateHeight(viewportHeight:Number, contentHeight:Number, queueY:Number):void
		{
			_localY = 0;
			_bg.height = viewportHeight;
			_handle.graphics.clear();
			_handle.graphics.beginFill(Main.DARK_COLOR);
			_handle.graphics.drawRoundRect(2.5,0,5,viewportHeight * (viewportHeight / contentHeight), 5,5);
			_handle.graphics.endFill();
			dispatchEvent(_scrollEvent);
			//onEnterFrame(null);
		}
	}
}