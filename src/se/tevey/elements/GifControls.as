package se.tevey.elements
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import se.tevey.events.ObjectEvent;
	
	public class GifControls extends Sprite
	{
		public static const EVENT_ADD_FRAME:String = "event/addframe";
		public static const EVENT_SAVE_GIF:String = "event/savegif";
		
		private var _addFrameBtn:DefaultButton;
		private var _saveBtn:DefaultButton;
		private var _title:TextField;
		private var _quality:InputField;
		private var _repeat:InputField;
		private var _delay:InputField;
		private var _bg:Sprite;
		
		public function GifControls()
		{
			super();
			init();
		}
		
		private function init():void
		{
			
			_bg = new Sprite();
			_bg.graphics.beginFill(Main.DARK_COLOR);
			_bg.graphics.drawRect(0,0, Main.SIDEBAR_WIDTH,150);
			_bg.graphics.endFill();
			_bg.graphics.beginFill(Main.LIGHT_COLOR);
			_bg.graphics.drawRect(0,0,Main.SIDEBAR_WIDTH, 20);
			
			var titleFormat:TextFormat = new TextFormat();
			titleFormat.align = TextFormatAlign.CENTER;
			titleFormat.color = Main.DARK_COLOR;
			titleFormat.font = "Trebuchet MS, Arial, Helvetica, sans-serif";
			titleFormat.size = 12;
			
			_title = new TextField();
			_title.defaultTextFormat = titleFormat;
			_title.text = "gif";
			_title.x = Main.MARGIN;
			_title.autoSize = TextFieldAutoSize.LEFT;
			
			_quality = new InputField("Quality(1-256):", 10, "Quality: 1 best but slow, 256 ugly but fast, 10 best of both worlds.");
			_quality.addEventListener(InputField.EVENT_INPUT_FOCUS, onInputFocus);
			_quality.y = 20 + Main.MARGIN;
			_quality.x = Main.MARGIN;
			
			_repeat = new InputField("Repeat:", 0, "Repeat: Number of times to loop the gif.");
			_repeat.addEventListener(InputField.EVENT_INPUT_FOCUS, onInputFocus);
			_repeat.y = _quality.y + _quality.height + 5;
			_repeat.x = Main.MARGIN;
			
			_delay = new InputField("Delay(ms):", 3000, "Delay: The delay between frames in milliseconds.");
			_delay.addEventListener(InputField.EVENT_INPUT_FOCUS, onInputFocus);
			_delay.y = _repeat.y + _repeat.height + 5;
			_delay.x = Main.MARGIN;
			
			_addFrameBtn = new DefaultButton("Add frame", onAddFrame);
			_addFrameBtn.y = _bg.height - _addFrameBtn.height - Main.MARGIN;
			_addFrameBtn.x = Main.MARGIN;
			
			_saveBtn = new DefaultButton("Save GIF", onSaveGif);
			_saveBtn.y = _addFrameBtn.y;
			_saveBtn.x = _bg.width - _saveBtn.width - Main.MARGIN;
			
			addChild(_bg);
			addChild(_title);
			addChild(_quality);
			addChild(_repeat);
			addChild(_delay);
			addChild(_addFrameBtn);
			addChild(_saveBtn);
		}
		
		private function onInputFocus(event:ObjectEvent):void
		{
			dispatchEvent(new ObjectEvent(InputField.EVENT_INPUT_FOCUS, event.object));
		}
		
		public function disable():void
		{
			_quality.disable();
			_repeat.disable();
			_delay.disable();
		}
		
		public function enable():void
		{
			_quality.enable();
			_repeat.enable();
			_delay.enable();
		}
		
		public function get quality():int
		{
			return Math.max(1, Math.min(parseInt(_quality.inputValue), 256));
		}
		
		public function get repeat():int
		{
			return parseInt(_repeat.inputValue);
		}
		
		public function get delay():int
		{
			return parseInt(_delay.inputValue);
		}
		
		private function onAddFrame(me:MouseEvent):void
		{
			dispatchEvent(new Event(EVENT_ADD_FRAME));
		}
		
		private function onSaveGif(me:MouseEvent):void
		{
			dispatchEvent(new Event(EVENT_SAVE_GIF));
		}
	}
}