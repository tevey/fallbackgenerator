package
{
	import flash.desktop.ClipboardFormats;
	import flash.desktop.ClipboardTransferMode;
	import flash.desktop.Icon;
	import flash.desktop.NativeApplication;
	import flash.desktop.NativeDragManager;
	import flash.display.BitmapData;
	import flash.display.InteractiveObject;
	import flash.display.JPEGEncoderOptions;
	import flash.display.Loader;
	import flash.display.PNGEncoderOptions;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.InvokeEvent;
	import flash.events.MouseEvent;
	import flash.events.NativeDragEvent;
	import flash.external.ExternalInterface;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.filters.DropShadowFilter;
	import flash.geom.Rectangle;
	import flash.html.HTMLLoader;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	import flash.system.Security;
	
	import org.bytearray.gif.encoder.GIFEncoder;
	import org.osmf.elements.HTMLElement;
	
	import se.tevey.DebugConsole;
	import se.tevey.elements.BannerInfo;
	import se.tevey.elements.GifControls;
	import se.tevey.elements.HelpWindow;
	import se.tevey.elements.ImageControls;
	import se.tevey.elements.InputField;
	import se.tevey.elements.Queue;
	import se.tevey.events.ObjectEvent;
	
	public class Main extends Sprite
	{
		[Embed(source="ficon.png")]
		private var Icon:Class;
		
		public static const DARK_COLOR:uint = 0x1e1e1e;
		public static const LIGHT_COLOR:uint = 0xe9f1df;
		public static const SIDEBAR_WIDTH:Number = 194;
		public static const MARGIN:Number = 10;
		
		private var _queue:Queue;
		private var _loader:Loader;
		private var _request:URLRequest;
		private var _files:Array;
		private var _mask:Sprite;
		private var _dimensions:Rectangle;
		private var _currentFile:File;
		private var _bmd:BitmapData;
		private var _bannerInfo:BannerInfo;
		private var _helpWindow:HelpWindow;
		private var _sidebarbg:Sprite;
		private var _fIcon:Sprite;
		private var _dropArea:Sprite;
		
		//private var _htmlLoader:HTMLLoader;
		private var _fileURL:String;
		
		//GIF
		private var _gifControls:GifControls;
		private var _gifEncoder:GIFEncoder;
		private var _firstFrame:Boolean = true;
		
		//IMAGE
		private var _imageControls:ImageControls;
		private var _jpegOptions:JPEGEncoderOptions;
		private var _pngOptions:PNGEncoderOptions;
		
		public function Main()
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{
			DebugConsole.initialize(stage);
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stage.color = DARK_COLOR;
			init();
		}
		
		private function onStageResize(event:Event):void
		{
			_dropArea.width = stage.stageWidth;
			_dropArea.height = stage.stageHeight;
			_queue.updateHeight(stage.stageHeight);
			_queue.x = stage.stageWidth - SIDEBAR_WIDTH;
			_gifControls.x = stage.stageWidth - SIDEBAR_WIDTH;
			_imageControls.x = stage.stageWidth - SIDEBAR_WIDTH;
			_bannerInfo.x = stage.stageWidth - SIDEBAR_WIDTH;
			_sidebarbg.x = stage.stageWidth - SIDEBAR_WIDTH;
			_fIcon.y = stage.stageHeight - _fIcon.height - MARGIN;
			_sidebarbg.height = stage.stageHeight;
			
			updatebannerPosition();
		}
		
		private function updatebannerPosition():void
		{
			if(_loader && _dimensions && _mask)
			{
				_dimensions.y = (stage.stageHeight - _dimensions.height) * 0.5;
				_dimensions.x = ((stage.stageWidth - SIDEBAR_WIDTH) - _dimensions.width) * 0.5;
				_mask.x = _dimensions.x;
				_mask.y = _dimensions.y;
				_loader.y = _dimensions.y;
				_loader.x = _dimensions.x;
				//_htmlLoader.x = _dimensions.x;
				//_htmlLoader.y = _dimensions.y;
			}
		}
		
		private function init():void
		{
			//_htmlLoader = new HTMLLoader();
			//_htmlLoader.load(new URLRequest("embed/template.html"));
			//addChild(_htmlLoader);
			
			_files = new Array();
			_request = new URLRequest();
			_dimensions = new Rectangle();
			
			_dropArea = new Sprite();
			_dropArea.graphics.beginFill(DARK_COLOR);
			_dropArea.graphics.drawRect(0,0,10,10);
			_dropArea.graphics.endFill();
			addChild(_dropArea);
			
			_sidebarbg = new Sprite();
			_sidebarbg.graphics.beginFill(DARK_COLOR);
			_sidebarbg.graphics.drawRect(0,0,SIDEBAR_WIDTH, 800);
			_sidebarbg.graphics.endFill();
			_sidebarbg.filters = [new DropShadowFilter(0,-90,0,1,10)];
			addChild(_sidebarbg);
			
			_fIcon = new Sprite();
			_fIcon.buttonMode = true;
			_fIcon.mouseChildren = false;
			_fIcon.addChild(new Icon());
			_fIcon.x = MARGIN;
			_fIcon.addEventListener(MouseEvent.MOUSE_DOWN, onIconClicked);
			addChild(_fIcon);
			
			//INFO
			_bannerInfo = new BannerInfo();
			_bannerInfo.addEventListener(BannerInfo.EVENT_NEXT_BANNER, onNextDown);
			_bannerInfo.y = 0;
			addChild(_bannerInfo);
			
			//IMAGE
			_pngOptions = new PNGEncoderOptions();
			_jpegOptions = new JPEGEncoderOptions(100);
			_imageControls = new ImageControls();
			_imageControls.addEventListener(ImageControls.EVENT_SAVE_JPEG, onSaveJpeg);
			_imageControls.addEventListener(ImageControls.EVENT_SAVE_PNG, onSavePng);
			_imageControls.addEventListener(InputField.EVENT_INPUT_FOCUS, onInputFocus);
			_imageControls.y = _bannerInfo.height;
			addChild(_imageControls);
			
			//GIF
			_gifControls = new GifControls();
			_gifControls.addEventListener(GifControls.EVENT_ADD_FRAME, onAddFrame);
			_gifControls.addEventListener(GifControls.EVENT_SAVE_GIF, onSaveGif);
			_gifControls.addEventListener(InputField.EVENT_INPUT_FOCUS, onInputFocus);
			_gifControls.y = _imageControls.y + _imageControls.height;
			addChild(_gifControls);
			
			_queue = new Queue();
			_queue.y = _gifControls.y + _gifControls.height;
			addChild(_queue);
			
			_mask = new Sprite();
			addChild(_mask);
			
			_helpWindow = new HelpWindow();
			_helpWindow.y = 10;
			_helpWindow.x = 50;
			addChild(_helpWindow);
			
			stage.addEventListener(Event.RESIZE, onStageResize);
			onStageResize(null);
			
			stage.addEventListener(NativeDragEvent.NATIVE_DRAG_ENTER, onDragEnter);
			stage.addEventListener(NativeDragEvent.NATIVE_DRAG_DROP, onDragDrop);
			NativeApplication.nativeApplication.addEventListener(InvokeEvent.INVOKE, onInvoke)
		}
		
		private function onIconClicked(event:MouseEvent):void
		{
			navigateToURL(new URLRequest("http://fallbackgenerator.com"), "_blank");
		}
		
		private function onInputFocus(event:ObjectEvent):void
		{
			if(event.object.toString().length > 0)
				_helpWindow.showMessage(event.object.toString());
		}
		
		private function onNextDown(event:Event):void
		{
			loadNext();
		}
		
		private function onDragEnter(event:NativeDragEvent):void
		{
			NativeDragManager.acceptDragDrop(stage);
		}
		
		private function onDragDrop(event:NativeDragEvent):void
		{
			var newItems:Array = event.clipboard.getData(ClipboardFormats.FILE_LIST_FORMAT) as Array;
			for (var i:int = 0; i < newItems.length; i++) 
			{
				if((newItems[i] as File).extension == "swf")
					_files.push(newItems[i]);
			}
			
			filesAdded();
		}
		
		private function onInvoke(event:InvokeEvent):void
		{
			var args:Array = event.arguments;
			for (var i:int = 0; i < args.length; i++) 
			{
				var file:File = new File(args[i]);
				if(file.extension == "swf")
					_files.push(file);
			}
			filesAdded();
		}
		
		private function filesAdded():void
		{
			updateQueue();
			clearLoader();
			if(_files[0])
				loadSwf(_files[0]);
		}
		
		private function updateQueue():void
		{
			_queue.clear();
			for (var i:int = 0; i < _files.length; i++) 
			{
				_queue.addToQueue((_files[i] as File).name, i);
			}
		}
		
		private function loadSwf(file:File):void
		{
			_request.url = file.url;
			_fileURL = file.url;
			
			_loader = new Loader();
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoadComplete);
			_loader.load(_request);
		}
		
		private function onLoadComplete(event:Event):void
		{
			_currentFile = (_files[0] as File);
			
			_bannerInfo.setData(_loader.contentLoaderInfo, _currentFile.name);
			
			_dimensions.width = _loader.contentLoaderInfo.width;
			_dimensions.height = _loader.contentLoaderInfo.height;
			
			//NEW work with later
			/*_htmlLoader.window.loadSWF(_fileURL, "flash", _loader.contentLoaderInfo.swfVersion.toString());
			_htmlLoader.width = _dimensions.width;
			_htmlLoader.height = _dimensions.height;
			setChildIndex(_htmlLoader, 0);*/
			
			_mask.graphics.clear();
			_mask.graphics.beginFill(0x00ff00);
			_mask.graphics.drawRect(0, 0 , _dimensions.width, _dimensions.height);
			_mask.graphics.endFill();
			
			updatebannerPosition();
			addChildAt(_loader, 1);
			_loader.mask = _mask;
		}
		
		private function onAddFrame(e:Event):void
		{
			if(_files.length == 0)
				return;
			
			if(_firstFrame)
			{
				_gifEncoder = new GIFEncoder();
				_gifEncoder.setQuality(_gifControls.quality);
				_gifEncoder.setRepeat(_gifControls.repeat);
				_gifEncoder.setDelay(_gifControls.delay);
				_gifEncoder.start();
				_firstFrame = false;
			}
			_gifControls.disable();
			_bmd = new BitmapData(_dimensions.width, _dimensions.height, false, 0xff0000);
			_bmd.draw(_loader.content);
			_gifEncoder.addFrame(_bmd);
		}
		
		private function onSaveGif(e:Event):void
		{
			if(_firstFrame)
			{
				_helpWindow.showMessage("Please add frames to the gif before saving");
				return;
			}
			
			_gifEncoder.finish();
			var file:File = new File(_currentFile.nativePath.replace(".swf", ".gif"));
			var fileStream:FileStream = new FileStream();
			fileStream.open(file, FileMode.WRITE);
			fileStream.writeBytes(_gifEncoder.stream);
			fileStream.close();
			_firstFrame = true;
			_gifControls.enable();
			_helpWindow.showMessage(file.name + " saved at " + file.nativePath);
		}
		
		private function onSaveJpeg(e:Event):void
		{
			if(_files.length == 0)
				return;
			
			_jpegOptions.quality = _imageControls.getQuality();
			_bmd = new BitmapData(_dimensions.width, _dimensions.height, false, 0xff0000);
			_bmd.draw(_loader.content);
			
			var file:File = new File(_currentFile.nativePath.replace(".swf", ".jpg"));
			var fileStream:FileStream = new FileStream();
			fileStream.open(file, FileMode.WRITE);
			fileStream.writeBytes(_bmd.encode(new Rectangle(0,0,_dimensions.width, _dimensions.height), _jpegOptions));
			fileStream.close();
			_helpWindow.showMessage(file.name + " saved at " + file.nativePath);
		}
		
		private function onSavePng(e:Event):void
		{
			if(_files.length == 0)
				return;
			
			_bmd = new BitmapData(_dimensions.width, _dimensions.height, false, 0xff0000);
			_bmd.draw(_loader.content);
			
			var file:File = new File(_currentFile.nativePath.replace(".swf", ".png"));
			var fileStream:FileStream = new FileStream();
			fileStream.open(file, FileMode.WRITE);
			fileStream.writeBytes(_bmd.encode(new Rectangle(0,0,_dimensions.width, _dimensions.height), _pngOptions));
			fileStream.close();
			_helpWindow.showMessage(file.name + " saved at " + file.nativePath);
		}
		
		private function loadNext():void
		{
			_bannerInfo.clear();
			_gifControls.enable();
			_firstFrame = true;
			_files.shift();
			updateQueue();
			clearLoader();
			
			if(_files[0])
				loadSwf(_files[0]);
		}
		
		private function clearLoader():void
		{
			if(_loader)
				if(this.contains(_loader))
					this.removeChild(_loader);
		}
	}
}