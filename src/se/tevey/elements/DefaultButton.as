package se.tevey.elements
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	public class DefaultButton extends Sprite
	{
		private var _text:TextField;
		private var _func:Function;
		
		public function DefaultButton(t:String, clickFunc:Function=null)
		{
			super();
			_func = clickFunc;
			
			var labelFormat:TextFormat = new TextFormat();
			labelFormat.color = Main.DARK_COLOR;
			labelFormat.font = "Trebuchet MS, Arial, Helvetica, sans-serif";
			labelFormat.size = 12;
			
			_text = new TextField();
			_text.defaultTextFormat = labelFormat;
			_text.autoSize = TextFieldAutoSize.LEFT;
			_text.text = t;
			
			graphics.beginFill(Main.LIGHT_COLOR, 1);
			graphics.drawRoundRect(0, 0, _text.textWidth + 10, _text.height + 2, 5, 5);
			graphics.endFill();
			_text.x = 3;
			_text.y = 1;
			_text.selectable = false;
			addChild(_text);
			addEventListener(MouseEvent.MOUSE_DOWN, onClick);
			buttonMode = true;
			mouseChildren = false;
		}
		
		private function onClick(me:MouseEvent):void
		{
			if (_func != null)
				_func(me);
		}
		
	}
}